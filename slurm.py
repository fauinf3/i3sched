import os
from subprocess import check_output as cmd
from subprocess import CalledProcessError
from subprocess import STDOUT

import xml.etree.ElementTree as ET

class SlurmError(Exception):
    def __init__(self, msg):
        self.msg = msg
    def __str__(self):
        return repr("%s" % (self.msg))


class Slurm(object):
    sbatch = "sbatch"
    squeue = "squeue"
    sacct = "sacct"
    scancel = "scancel"
    sinfo = "sinfo"
    
    idleCmd = "echo '/bin/sleep 3144960000'"
    
    standardPartition = "on.q"

    def __init__(self, sbatch="sbatch", squeue="squeue", sacct="sacct",
            scancel="scancel", sinfo="sinfo", idleCmd="echo '/bin/sleep 3144960000'",         
            standardPartition = "on.q"):
        
        self.sbatch = sbatch
        self.squeue = squeue
        self.scancel = scancel
        self.sinfo = sinfo
        self.sacct = sacct

        self.idleCmd = idleCmd
        
        self.standardPartition = standardPartition
                
        self.jobs = []
        self.runningJobs = []
        self.pendingJobs = []
        self.deletedJobs = []
        self.finishedJobs = []       
        self._receiveJobs()
    
    def reloadJobs(self):
        self.jobs = []
        self.runningJobs = []
        self.pendingJobs = []
        self.deletedJobs = []
        self.finishedJobs = []
        self._receiveJobs()
    
    def _receiveJobs(self):
        try:
            running = cmd("%s -o '%%i %%P %%j %%u %%t %%M %%D %%R' -h -u `whoami` --states=R,CG" % self.squeue, shell=True, stderr=STDOUT)
            finished = cmd("%s -n -o 'jobid,jobname,account,partition,alloccpus,elapsed,state,exitcode'" % self.sacct, shell=True, stderr=STDOUT) 
            pending = cmd("%s -o '%%i %%P %%j %%u %%t %%M %%D %%R' -h -u `whoami` --states=PD" % self.squeue, shell=True, stderr=STDOUT)
        except OSError:
            raise SlurmError("squeue command \"%s\" not found" % self.squeue)
        except CalledProcessError, error:
            raise SlurmError("squeue failed: could not receive Slurm jobs: %s" % error.output.strip())


        for jobString in running.split('\n'):
	    if not jobString=='': 
                job = SlurmJob(jobString, False)
                self.jobs.append(job)
                self.runningJobs.append(job)

        for jobString in pending.split('\n'):
	    if not jobString=='':
                job = SlurmJob(jobString, False)
                self.jobs.append(job)
                self.pendingJobs.append(job)

        for jobString in finished.split('\n'):
	    if not (jobString=='' or jobString.startswith("Conflicting")):
                job = SlurmJob(jobString, True)
                self.jobs.append(job)
                self.finishedJobs.append(job)


    def getQueueHosts(self, requiredPartition=None):
        if requiredPartition is None:
             requiredPartition = self.standardPartition
        
        try:
            output = cmd("%s -h -lNe -p %s -o '%%n'" % (self.sinfo,requiredPartition), shell=True, stderr=STDOUT)
        except OSError:
            raise SlurmError("sinfo command \"%s\" not found" % self.sinfo)
        except CalledProcessError, error:
            raise SlurmError("sinfo failed: %s" % error.output.strip())
       
        
        agreedHosts = []
        hosts = output.split("\n")
	for host in hosts:
	   if not host=='':
	       agreedHosts.append(host)
 
        return(agreedHosts)



    def getAllJobs(self):
        return self.jobs
    
    def getPendingJobs(self):
        return self.pendingJobs
    
    def getRunningJobs(self):
        return self.runningJobs

    def getFinishedJobs(self):
        return self.finishedJobs

    def submitJob(self, name, excludedHosts=[], partition=None, hostSuffix=None, memory=None, cpu=None, parallelEnvironment=None, stdout="/dev/null", stderr="/dev/null"):
        if partition is None:
            partition = self.standardPartition

        if len(excludedHosts) == 0:
            excludedNodes = ""
        else:
            excludedNodes = "-x "+",".join([host for host in excludedHosts])

        if memory is None:
            memory = ""
        else:
            memory = "--mem=%s" % memory
        
        if cpu is None:
            cpu = ""
        else:
            cpu = "-n%s" % cpu
        sbatchArgs = "-J \"%s\" %s -p %s %s %s -o %s -e %s -t 11520" % (name, excludedNodes, partition, memory, cpu, stdout, stderr)

        try:
	    #print "%s | %s %s" % (self.idleCmd, self.sbatch, sbatchArgs)
            output = cmd("%s | %s %s" % (self.idleCmd, self.sbatch, sbatchArgs), shell=True, stderr=STDOUT)
        except OSError:
            raise SlurmError("sbatch command \"%s\" not found" % self.qsub)
        except CalledProcessError, error:
            raise SlurmError("sbatch failed: %s" % error.output.strip())

        output = output.strip()
        if output.endswith("has been submitted"):
            jobId = output.split(" ")[2]
            return (True, jobId)
        else:
            return (False, output)


    def deleteJob(self, name="", id=0):
        if id == 0 and name == "":
            return (False, "No ID or name given")
        
        if id != 0:
            delete = str(id)
        else:
            delete = "-n "+name
            
        try:
            output = cmd("%s \"%s\"" % (self.scancel, delete), shell=True, stderr=STDOUT)
        except OSError:
            raise SlurmError("scancel command \"%s\" not found" % self.scancel)
        except CalledProcessError, error:
            raise SlurmError("scancel failed: %s" % error.output.strip())

        output = output.strip()
        if output == '':            
            return (True, str(id))
        else:
            return (False, output)


class SlurmJob(object):

    def __init__(self, jobString, finished):
        fields = jobString.split()

        
        self.id = fields[0]
        self.name = fields[2]

	if not finished:
	    state = fields[4]
            self.username = fields[3]
	    runningStates = ["R","CG"]
	    pendingStates = ["PD"]
            if state in runningStates:
	        self.state = "running"            
            elif state in pendingStates:
                self.state = "pending"
        
            if not fields[7].startswith("("):
                self.hostname = fields[7]
	    else:
	        self.hostname = ""
	else:
	    self.state = "finished"
	    self.hostname = ""

